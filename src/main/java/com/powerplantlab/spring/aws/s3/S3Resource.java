package com.powerplantlab.spring.aws.s3;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import org.springframework.core.io.AbstractFileResolvingResource;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author Sigmund Lundgren
 * @since 2011-09-16 15:04
 */
public class S3Resource extends AbstractFileResolvingResource {

    private String uri;

    private String bucket;
    private String key;

    private AmazonS3 amazonS3;
    private AWSCredentials awsCredentials;

    public S3Resource(String uri) throws IOException {
        this.uri = uri;
        this.bucket = getBucketFromURI(uri);
        this.key = getKeyFromURI(uri);
        awsCredentials = getCredentials();
        amazonS3 = new AmazonS3Client(awsCredentials);
    }

    private String getKeyFromURI(String uri) {
        throw new UnsupportedOperationException();
    }

    private String getBucketFromURI(String uri) {
        throw new UnsupportedOperationException();
    }

    private PropertiesCredentials getCredentials() throws IOException {
        return new PropertiesCredentials(new ClassPathResource("aws-credentials").getInputStream());
    }

    public void setAmazonS3(AmazonS3 amazonS3) {
        this.amazonS3 = amazonS3;
    }

    public String getDescription() {
        throw new UnsupportedOperationException();
    }

    public InputStream getInputStream() throws IOException {
        return amazonS3.getObject(bucket, key).getObjectContent();
    }
}
