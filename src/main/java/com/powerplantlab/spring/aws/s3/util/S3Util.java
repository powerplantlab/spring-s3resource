package com.powerplantlab.spring.aws.s3.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Sigmund Lundgren
 * @since 2011-09-23 10:21
 */
public class S3Util {
    private static final String s3CaptureRegexp = "(^[s,S]3:\\/{2})(\\w+)";
    private static final String s3URIValidatorRegexp = "^[s,S]3:\\/{2}\\S+";

    private static final Pattern capturePattern = Pattern.compile(s3CaptureRegexp);


    public static String getBucketFromURI(final String uri) {
        return checkAndReturnGroup(uri, 2);
    }

    public static String getPathFromURI(final String uri) {
        String protocolAndBucket =  checkAndReturnGroup(uri, 0);
        return protocolAndBucket.equals("") ? "" : uri.replaceAll(protocolAndBucket, "");
    }

    private static String checkAndReturnGroup(final String uri, final int regexpGroup) {
        if(!uri.matches(s3URIValidatorRegexp)) {
            throw new IllegalArgumentException("Not a valid S3 URI");
        }
        Matcher captureMatcher = capturePattern.matcher(uri);
        if(!captureMatcher.find() || captureMatcher.groupCount() < 2) {
            return "";
        }
        return captureMatcher.group(regexpGroup);
    }
}
