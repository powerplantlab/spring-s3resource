package com.powerplantlab.spring.aws.s3;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Sigmund Lundgren
 * @since 2011-09-16 15:05
 */
public class S3ResourceTest {

    @Mock
    private AmazonS3 amazonS3Client;

    private S3Resource s3Resource;
    private S3Object s3Object;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        s3Resource = new S3Resource("s3://mybucket/test.properties");
        s3Resource.setAmazonS3(amazonS3Client);

        File propertiesFile = new ClassPathResource("test.properties").getFile();
        InputStream inputStream = new FileInputStream(propertiesFile);

        s3Object = new S3Object();
        s3Object.setObjectContent(inputStream);
    }

    @Test
    public void testCreate() throws Exception {
        when(amazonS3Client.getObject("s3://mybucket", "test.properties")).thenReturn(s3Object);

        s3Resource.getInputStream();

        verify(amazonS3Client).getObject("s3://mybucket", "test.properties");
    }
}
