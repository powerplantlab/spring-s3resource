package com.powerplantlab.spring.aws.s3.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Sigmund Lundgren
 * @since 2011-09-23 10:21
 */
public class S3UtilTest {

    @Test
    public void testGetBucketFromURI() throws Exception {
        assertEquals("mybucket", S3Util.getBucketFromURI("s3://mybucket/test.properties"));
        assertEquals("mybucket", S3Util.getBucketFromURI("s3://mybucket"));
        assertEquals("", S3Util.getBucketFromURI("s3:///"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetBucketFromURIIllegalURI() throws Exception {
        S3Util.getBucketFromURI("http://mybucket/test.properties");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetBucketFromURIIllegalURIMissingSlasheds() throws Exception {
        S3Util.getBucketFromURI("s3:mybucket/test.properties");
    }

    @Test
    public void testGetPathFromURI() throws Exception {
        assertEquals("/en/test.properties", S3Util.getPathFromURI("s3://mybucket/en/test.properties"));
        assertEquals("/en/", S3Util.getPathFromURI("s3://mybucket/en/"));
        assertEquals("/", S3Util.getPathFromURI("s3://mybucket/"));
        assertEquals("", S3Util.getPathFromURI("s3://mybucket"));
        assertEquals("", S3Util.getPathFromURI("s3:///"));
    }

    @Test
    public void testGetPathFromURI2() throws Exception {
        assertEquals("", S3Util.getPathFromURI("s3:///"));
    }
}
